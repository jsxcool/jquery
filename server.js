var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(express.static(__dirname + "/public"));  // middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var USERS = [
  {
    name: 'Bob',
    age: 21
  },
  {
    name: 'Jack',
    age: 25
  },
  {
    name: 'Alice',
    age: 22
  }
];

var router = express.Router();

router.get("/cal/add/:x/:y", function(req, res){
  var x = +req.params.x;
  var y = +req.params.y;
  setTimeout(function(){
    res.send("" + (x+y));
  }, 1000)
})

router.get("/cal/multiply/:x/:y", function(req, res){
  var x = +req.params.x;
  var y = +req.params.y;
  setTimeout(function(){
    res.send("" + (x*y));
  }, 1500)
})

router.post("/cal/add", function(req, res){
  var x = +req.body.x;
  var y = +req.body.y;
  setTimeout(function(){
    res.send("" + (x+y));
  }, 1000)
})

router.post("/cal/multiply", function(req, res){
  var x = +req.body.x;
  var y = +req.body.y;
  setTimeout(function(){
    res.send("" + (x*y));
  }, 1500)
})

router.get("/users", function(req, res){
  res.send(USERS);
})

router.post("/users", function(req, res){
  var name = req.body.name;
  var age = req.body.age;
  if(name !== "" && age !== ""){
    USERS.push({
      name: name,
      age: age
    })
  }
  console.log(USERS)
  res.send(USERS);
})

router.post("/usersDelete", function(req, res){
  var myname = req.body.name;
  var myage = req.body.age;
  for(var i = 0; i< USERS.length; i++){
    if(USERS[i].name == myname && USERS[i].age == myage){
      USERS.splice(i,1);
      break;
    }
  }
  console.log(USERS)
  res.send(USERS);
})

app.use("/", router);

// error handling
app.use(function(req, res, next){
  res.end("404 - Request can't be found")
});

app.listen(3008);
console.log("server started!");
