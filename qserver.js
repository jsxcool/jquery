var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(express.static(__dirname + "/public"));  // middleware
app.use(bodyParser.urlencoded({extend: false}));
app.use(bodyParser.json());

var USERS = [
   {
      name: 'Bob',
      age: 21
   },
   {
      name: 'Jack',
      age: 25
   },
   {
      name: 'Alice',
      age: 22
   }
];

var router = express.Router();
router.get("/cal/add/:x/:y", function(req, res) {
   var x = +req.params.x; // * 1
   var y = +req.params.y;
   setTimeout(function() {
      res.send("" + (x + y));
   }, 1000);
});

router.get("/cal/multiply/:x/:y", function(req, res) {
   var x = +req.params.x; // * 1
   var y = +req.params.y;
   setTimeout(function() {
      res.send("" + (x * y));
   }, 1500);
});

router.post("/cal/add", function(req, res) {
   var x = +req.body.x; // * 1
   var y = +req.body.y;
   setTimeout(function() {
      res.send("" + (x + y));
   }, 1000);
});

router.post("/cal/multiply", function(req, res) {
   var x = +req.body.x; // * 1
   var y = +req.body.y;
   setTimeout(function() {
      res.send("" + (x * y));
   }, 1500);
});

router.get("/users", function(req, res) {
   res.send(USERS);
});

router.post("/users/add", function(req, res) {
   var name = req.body.name;
   var age = req.body.age;
   if (name !== "" && age !== "") {
      USERS.push({
         name: name,
         age: age
      })
   }
   res.send(USERS);
});

router.post("/users/remove", function (req, res) {
   var name = req.body.name;
   var age = req.body.age;
   if (name !== "" && age !== "") {
      USERS.pop({
         name: name,
         age: age
      })
   }
   res.send(USERS);
});

app.use("/", router);

// error handling.
app.use(function(req, res, next) {
   res.end("404 - Request can't be handled.");
});

app.listen(3008);
console.log("server started");
